angular.module('starter.controllers')

.controller('TeacherCtrl', function($scope, $window, FirebaseDb, $ionicPopup, $state, $stateParams, $rootScope, $http, $ionicHistory, $cordovaCamera, $ionicModal) {


    //$window.innerHeight
    //$window.innerHeight

    $rootScope.schoolid = 1;
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true;
    $scope.documentType = {};
    $scope.showOzluk = true;
    $scope.showDocs = false;
    $scope.showSalary = false;
    $scope.showPayments = false;
    $scope.documentImage = "";


    $scope.payment = {};
    $scope.payment.amount = 0;
    $scope.payment.note = "";
    $scope.payment.date = new Date();

    // dates variables for input.
    $scope.dates = {};
    $scope.dates.vizeBaslangic = new Date();
    $scope.dates.vizeBitis = new Date();
    $scope.dates.birthDate = new Date();
    $scope.dates.workBegin = new Date();
    $scope.dates.workEnd = new Date();

    $ionicModal.fromTemplateUrl('templates/addDocument.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: true,
        hardwareBackButtonClose: true,
        focusFirstInput: true
    }).then(function(modal) {
        $scope.modal1 = modal;
        console.log($scope.modal1);
    });

    $ionicModal.fromTemplateUrl('templates/teacher/payment.html', {
        scope: $scope,
        animation: 'slide-in-up',
        backdropClickToClose: true,
        hardwareBackButtonClose: true,
        focusFirstInput: true
    }).then(function(modal) {
        $scope.modal2 = modal;
        console.log($scope.modal2);
    });

    $scope.openModal = function(index) {
        if (index == 1) {
            $scope.document = {};
            $scope.modal1.show();
        }
        if (index == 2) {
            $scope.modal2.show();
        }
    };
    $scope.closeModal = function(index) {
        if (index == 1) {
            $scope.modal1.hide();
        }
        if (index == 2) {
            $scope.modal2.hide();
        }
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function(index) {
        $scope.modal1.remove();
        $scope.modal2.remove();
    });

    $scope.addPaymet = function(payment) {
        payment.date = getDateFromString(payment.date);
        payment.payee = "Cenker Hoca";
        FirebaseDb.addPayment('teachers', $scope.item.$id, payment);
        $scope.payment = {};
        $scope.payment.amount = 0;
        $scope.payment.note = "";
        $scope.payment.date = new Date();
         $scope.closeModal(2);
    }

    $scope.takePicture = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.item.image = "data:image/jpeg;base64," + imageData;
        }, function(err) {
            // An error occured. Show a message to the user
        });
    }

    function getItem(id) {

        //  when model is change, convert string date into dates variable.
        $scope.$watch('item', function() {
            $scope.dates = {};
            $scope.dates.vizeBaslangic = setDateFromString($scope.item.vizeBaslangic);
            $scope.dates.vizeBitis = setDateFromString($scope.item.vizeBitis);
            $scope.dates.birthDate = setDateFromString($scope.item.birthDate);
            $scope.dates.workBegin = setDateFromString($scope.item.workBegin);
            $scope.dates.workEnd = setDateFromString($scope.item.workEnd);
        }, true);

        $scope.item = FirebaseDb.get('teachers', id);

    };

    function getItems() {
        $scope.items = FirebaseDb.all('teachers');
    };

    function setDateFromString(s) {
        var sd = new String(s);
        var d = sd.split('-');
        return new Date(d[2], d[1] - 1, d[0]);
    }

    function getDateFromString(d) {
        return d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
    }

    $scope.addItem = function(item, dates) {
        item.vizeBaslangic = getDateFromString(dates.vizeBaslangic);
        item.vizeBitis = getDateFromString(dates.vizeBitis);
        item.birthDate = getDateFromString(dates.birthDate);
        item.workBegin = getDateFromString(dates.workBegin);
        item.workEnd = getDateFromString(dates.workEnd);
        FirebaseDb.save('teachers', item);
    }
    $scope.deleteItem = function(item) {

        var tablename = 'teacher';

        // ask for delete
        confirmPopup.then(function(res) {
            if (res) {

            }
        })
    };

    if ($stateParams.Id) {
        getItem($stateParams.Id);
    } else {
        getItems();
    }
})