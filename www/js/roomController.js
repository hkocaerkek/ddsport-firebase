angular.module('starter.controllers')

  .controller('RoomCtrl', function ($scope, $ionicPopup,  $stateParams, $rootScope, $http) {

    $scope.tableName = 'rooms';
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true

    function getItem(id) {

      Db.get($scope.tableName, id).then(function (result) {
        $scope.item = result;
      },
        function (error) { alert(error); })
        .finally(function () {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        })
    };

    function getItems() {

      Db.getAll($scope.tableName).then(function (result) {
        $scope.items = result;
      }, function (error) {
        alert(error);
      }).finally(function () {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      })
    };

    $scope.doRefresh = function () {
      getItems();

    };

    $scope.addItem = function (item) {
      item.schoolId = $rootScope.schoolid;
      Db.add($scope.tableName, item).then(function (params) {
        item = null;
      });
    }

    $scope.deleteItem = function (id, name, surname) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete teacher!',
        template: 'Are you sure you want to delete ' + name + ' ?'
      });

      confirmPopup.then(function (res) {
        if (res) {

          var item = Db.get(id);
          Db.delete($scope.tableName, item).then(function (result) {
            // get all items.
          }, function (error) {
            alert(error);
          }).finally(function () {
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
          })
        }
      });
    };

    if ($stateParams.Id) {

      getItem($stateParams.Id);

    }
    else {
      getItems();
    }
  })