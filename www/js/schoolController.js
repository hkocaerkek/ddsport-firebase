angular.module('starter.controllers')

.controller('SchoolCtrl', function($scope, Schools, $ionicPopup,  $rootScope, $stateParams, $http, $ionicUser) {

    $rootScope.schoolid = 1;
    $scope.profile = $rootScope.profile;
    $scope.tableName = 'schools';
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true

    function getItem(id) {
        $rootScope.schoolid = id;
        Db.get($scope.tableName, id).then(function(result) {
                    $scope.item = result;
                    $rootScope.schoolname = $scope.item.name;
                },
                function(error) {
                    console.log(error);
                    alert(error);
                })
            .finally(function() {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            })
    };

    function getItemWithFilter(filter) {

        Db.getChilds('schools', $rootScope.schoolid, 'classes').then(function(result) {
            $scope.classes = result;
        }, function(error) {
            console.log(error);
            alert(error);
        })

        Db.getChilds('schools', $rootScope.schoolid, 'rooms').then(function(result) {
            $scope.rooms = result;
        }, function(error) {
            console.log(error);
            alert(error);
        })

    };

    function getItems() {
        $scope.items = FirebaseDb.all('schools');
    };

    function getAllTeachersInTheSchool(id) {
        Db.getChilds('schools', $rootScope.schoolid, 'teachers').then(function(result) {
            $scope.personel = result;
        }, function(error) {
            alert(error);
        });
    }

    function getAllStudentsInTheSchool(id) {
        Db.getChilds('schools', $rootScope.schoolid, 'students').then(function(result) {
            $scope.students = result;
        }, function(error) {
            alert(error);
        });
    }

    $scope.doRefresh = function() {
        getItems();

    };

    $scope.addItem = function(item) {
        Db.add($scope.tableName, item).then(function(params) {
            item = null;
        });
    }

    $scope.deleteItem = function(id, name, surname) {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete teacher!',
            template: 'Are you sure you want to delete ' + name + ' ' + surname + ' ?'
        });

        confirmPopup.then(function(res) {
            if (res) {

                var item = Db.get(id);
                Db.delete($scope.tableName, item).then(function(result) {
                    // get all items.
                }, function(error) {
                    alert(error);
                }).finally(function() {
                    // Stop the ion-refresher from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                })
            }
        });
    };

    // for tabs
    $scope.tab = 3;

    $scope.setTab = function(newTab) {
        $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum) {
        return $scope.tab === tabNum;
    };


    if ($stateParams.Id) {

        getItem($stateParams.Id);
        filter = {
            'schoolId': $stateParams.Id
        };
        getItemWithFilter($rootScope.schoolid);
        getAllTeachersInTheSchool($stateParams.Id);
        getAllStudentsInTheSchool();
    } else {
        getItems();
    }

    $scope.calendar = {};
    $scope.changeMode = function(mode) {
        $scope.calendar.mode = mode;
    };

    $scope.loadEvents = function() {
        $scope.calendar.eventSource = createRandomEvents();
    };

    $scope.onEventSelected = function(event) {
        console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    };

    $scope.onViewTitleChanged = function(title) {
        $scope.viewTitle = title;
    };

    $scope.today = function() {
        $scope.calendar.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.calendar.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.onTimeSelected = function(selectedTime, events) {
        console.log('Selected time: ' + selectedTime + ', hasEvents: ' + (events !== undefined && events.length !== 0));
    };


    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }
        }
        return events;
    }


})