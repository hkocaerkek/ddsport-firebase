angular.module('starter.controllers', [ 'ionic', 'ionic-modal-select', 'ngCordova'])

.controller('AppCtrl', function ($scope, $rootScope, $state, $ionicModal, $timeout, $ionicAuth, $ionicUser) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $rootScope.profile = {};
  $rootScope.profile.name = "none";
  $rootScope.profile.isAdmin = false;
  $rootScope.profile.isSchool = false;
  $rootScope.profile.isTeacher = false;
  $rootScope.profile.isStudent = false;

  // Form data for the login modal
  $scope.loginData = {};


  // Triggered in the login modal to close it
  $scope.loginAs = function (profileName) {
    $rootScope.profile.name = profileName;

    $rootScope.profile.name = "none";
    $rootScope.profile.isAdmin = profileName == 'admin';
    $rootScope.profile.isSchool = profileName == 'school';
    $rootScope.profile.isTeacher = profileName == 'teacher';
    $rootScope.profile.isStudent = profileName == 'student';

    if (profileName == '') {
      login();
    } else {
      $state.go('app.' + profileName + 'main', {}, {
        reload: true
      });
    }
  };

  // Open the login modal
  $scope.login = function () {
    //  $scope.modal.show();
    $ionicAuth.login('google').then(function (result) {

        var user = $ionicUser.get();

        $scope.social = $ionicUser.social;
        $rootScope.profile.name = "admin";
      },
      function (error) {
        alert(error);
      });

  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function () {
    console.log('Doing login', $scope.loginData);


    var loginData = {
      'id': $scope.loginData.username,
      'passphrase': $scope.loginData.passphrase
    };
    var loginOptions = {
      'inAppBrowserOptions': {
        'hidden': true
      }
    };

    $ionicAuth.login('custom', loginData, loginOptions).then(function (result) {
        alert(result)
      },
      function (error) {
        alert(error);
      });

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function () {
      $scope.closeLogin();
    }, 1000);
  };
});