// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html) 
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var myApp = angular.module('starter', ['ionic', 'firebase', 'ionic.cloud', 'starter.controllers', 'ionic-modal-select', 'ngCordova', 'jett.ionic.filter.bar', 'ionic-color-picker', 'angularMoment'])

.factory("FirebaseDb", function($firebaseArray, $firebaseObject) {

    return {
        all: function(section) {
            var ref = new Firebase("https://extrareversi.firebaseio.com/" + section);
            var data = $firebaseArray(ref);
            return data;
        },

        get: function(section, id) {
            var ref = new Firebase("https://extrareversi.firebaseio.com");
            var data = $firebaseObject(ref.child(section).child(id));
            return data;
        },

        save: function(section, item) {
            var ref = new Firebase("https://extrareversi.firebaseio.com/" + section);

            if (item.$id == undefined) {
                itemsList = $firebaseArray(ref);
                itemsList.$add(item);
            } else {
                var id = item.$id;
                delete item.$$hashKey;
                delete item.$$conf;
                delete item.$id;
                delete item.$priority;
                var newItem = angular.copy(item);
                return ref.child(id).set(newItem);
            }
        },

        addDocuments: function(section, id, item) {
            var ref = new Firebase("https://extrareversi.firebaseio.com/" + section + "/" + id + "/documents");
            itemsList = $firebaseArray(ref);
            itemsList.$add(item);
        },

        addPayment: function(section, id, item) {
            var ref = new Firebase("https://extrareversi.firebaseio.com/" + section + "/" + id + "/payments");
            itemsList = $firebaseArray(ref);
            itemsList.$add(item);
        }
    };
})

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicFilterBarConfigProvider, $ionicCloudProvider) {

    $ionicCloudProvider.init({
        "core": {
            "app_id": "4f7249eb"
        }
    });

    $stateProvider

        .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })


    .state('app.calendar', {
        url: '/calendar',
        views: {
            'menuContent': {
                templateUrl: 'templates/calendar.html'
            }
        }
    })

    .state('app.mainpage', {
        url: '/mainpage',
        views: {
            'menuContent': {
                templateUrl: 'templates/mainpage.html'
            }
        }
    })

    .state('app.admin', {
        url: '/admin',
        views: {
            'menuContent': {
                templateUrl: 'templates/admin/admin.html',
                controller: 'AdminCtrl'
            }
        }
    })



    // School Begin
    .state('app.schools', {
        url: '/schools',
        views: {
            'menuContent': {
                templateUrl: 'templates/school/schools.html',
            }
        }
    })

    .state('app.school', {
            url: '/schools/:Id',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'templates/school/school.html',
                    controller: 'SchoolCtrl'
                }
            }
        })
        .state('app.updateschool', {
            url: '/updateschool/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/school/school-add.html',
                    controller: 'SchoolCtrl'
                }
            }
        })
        .state('app.addschool', {
            url: '/addschool',
            views: {
                'menuContent': {
                    templateUrl: 'templates/school/school-add.html',
                    controller: 'SchoolCtrl'
                }
            }
        })

    .state('app.addclass', {
        url: '/addclass',
        views: {
            'menuContent': {
                templateUrl: 'templates/class/class-add.html',
                controller: 'ClassCtrl'
            }
        }
    })

    .state('app.class', {
        url: '/class/:Id',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/class/class.html',
                controller: 'ClassCtrl'
            }
        }
    })

    .state('app.addroom', {
        url: '/addroom',
        views: {
            'menuContent': {
                templateUrl: 'templates/room/room-add.html',
                controller: 'RoomCtrl'
            }
        }
    })

    .state('app.room', {
        url: '/room/:Id',
        views: {
            'menuContent': {
                templateUrl: 'templates/room/room.html',
                controller: 'RoomCtrl'
            }
        }
    })

    // School End

    // Student Begin

    .state('app.students', {
        url: '/students',
        views: {
            'menuContent': {
                templateUrl: 'templates/student/students.html',
                controller: 'StudentCtrl'
            }
        }
    })

    .state('app.student', {
            url: '/students/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/student/student.html',
                    controller: 'StudentCtrl'
                }
            }
        })
        .state('app.updatestudent', {
            url: '/updatestudent/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/student/student-add.html',
                    controller: 'StudentCtrl'
                }
            }
        })
        .state('app.addstudent', {
            url: '/addstudent',
            views: {
                'menuContent': {
                    templateUrl: 'templates/student/student-add.html',
                    controller: 'StudentCtrl'
                }
            }
        })

    // Student End

    // Class Begin

    .state('app.classes', {
        url: '/classes',
        views: {
            'menuContent': {
                templateUrl: 'templates/class/classes.html'

            }
        }
    })


    .state('app.updateclass', {
        url: '/updateclass/:Id',
        views: {
            'menuContent': {
                templateUrl: 'templates/class/class-add.html',
                controller: 'ClassCtrl'
            }
        }
    })


    // Class End

    // Teacher Begin

    .state('app.teachers', {
        url: '/teachers',
        views: {
            'menuContent': {
                templateUrl: 'templates/teacher/teachers.html'

            }
        }
    })

    .state('app.teacher', {
            url: '/teachers/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/teacher/teacher.html',
                    controller: 'TeacherCtrl'
                }
            }
        })
        .state('app.updateteacher', {
            url: '/updateteacher/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/teacher/teacher-add.html',
                    controller: 'TeacherCtrl'
                }
            }
        })
        .state('app.addteacher', {
            url: '/addteacher/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/teacher/teacher-add.html',
                    controller: 'TeacherCtrl'
                }
            }
        })

    // Teacher End

    // Branch Begin

    .state('app.branches', {
        url: '/branches',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/branch/branches.html'

            }
        }
    })

    .state('app.branch', {
            url: '/branch/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/branch/branch.html',
                    controller: 'BranchCtrl'
                }
            }
        })
        .state('app.updatebranch', {
            url: '/updatebranch/:Id',
            views: {
                'menuContent': {
                    templateUrl: 'templates/branch/branch-add.html',
                    controller: 'BranchCtrl'
                }
            }
        })
        .state('app.addbranch', {
            url: '/addbranch',
            views: {
                'menuContent': {
                    templateUrl: 'templates/branch/branch-add.html',
                    controller: 'BranchCtrl'
                }
            }
        })

    // Branch End


    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/mainpage');
    //You can override the config such as the following


    $ionicFilterBarConfigProvider.theme('calm');
    $ionicFilterBarConfigProvider.clear('ion-close');
    $ionicFilterBarConfigProvider.search('ion-search');
    $ionicFilterBarConfigProvider.backdrop(false);
    $ionicFilterBarConfigProvider.transition('vertical');
    $ionicFilterBarConfigProvider.placeholder('Filter');


});