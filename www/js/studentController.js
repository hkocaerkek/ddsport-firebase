angular.module('starter.controllers')

  .controller('StudentCtrl', function ($scope, $rootScope, $ionicPopup,    $stateParams, $window, $state, $ionicHistory, $http, $ionicFilterBar) {

    $scope.tableName = 'students';
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true
    var filterBarInstance;

    $scope.showFilterBar = function () {
      filterBarInstance = $ionicFilterBar.show({
        items: $scope.items,
        update: function (filteredItems, filterText) {
          $scope.items = filteredItems;
          if (filterText) {
            console.log(filterText);
          }
        }
      });
    };

    function getItem(id) {
      Db.get($scope.tableName, id).then(function (result) {
        $scope.item = result;
      },
        function (error) { alert(error); })
        .finally(function () {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        })
    };

    function getItems() {
      Db.getAll($scope.tableName).then(function (result) {
        $scope.items = result;
      }, function (error) {
        alert(error);
      }).finally(function () {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      })
    };

    $scope.doRefresh = function () {
      getItems();
    };

    // students must belongs to a school, 
    // students can belongs to a class
    $scope.addItem = function (item) {
      // add student to the school
      if ($rootScope.schoolid == null) {
        alert("You must add student to a school");
        return;
      }
      item.schoolid = $rootScope.schoolid;
      Db.add($scope.tableName, item).then(function (params) {

        if (item.id == null) {  // if this is an insert, clear form.
          // alert("student " + item.name + " added.");
          //getItems();

          // if classid != null we must add this student to classstudent table.
          if ($rootScope.classid != null) {
            var classstudent = { classid: $rootScope.classid, studentid : params.id };
            Db.add('classstudent', classstudent).then(function (params) {
              // then return to class page
              $ionicHistory.goBack();
              $state.go('app.class',{Id: $rootScope.classid}, { reload: true });
            });
          }
          //
          //$ionicHistory.nextViewOptions({disableBack: true});
          //$window.location = "/#/app/students" ;// + params.id;
        }
        else {
          alert("student " + item.name + " updated.");
        }
      },
        function (error) { alert(error); });
    }

    $scope.deleteItem = function (item) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete student!',
        template: 'Are you sure you want to student ' + item.name + ' ' + item.surname + ' ?'
      });

      confirmPopup.then(function (res) {
        if (res) {

          Db.get('student', item.id).then(function (result) {
            var obj = result;
            //
            Db.deleteWithId('student', obj).
              then(function (result) {
                // get items.
                getItems();
              },
              function (error) { alert(error); }
              )

          },
            function (error) { alert(error); });
        }
      });
    };

    $scope.addRandomItem = function () {
      var URL = "https://randomuser.me/api/";
      $http.get(URL).then(function (resp) {
        console.log('Success', resp); // JSON object
        var obj = resp.data.results[0];
        $scope.addItem({
          name: obj.name.first,
          surname: obj.name.last,
          phone: obj.phone,
          imageUrl: obj.picture.large,
          thumbnailUrl: obj.picture.thumbnail,
          street: obj.location.street,
          city: obj.location.city,
          state: obj.location.state,
          postcode: obj.location.postcode,
          email: obj.email,
          gender: obj.gender,
          username: obj.username,
          password: obj.password
        });
      }, function (err) {
        console.error('ERR', err);
      });
    }

    // 
    $scope.addWeight = function(value)
    {
      $scope.weight = $scope.weight + value;
    };

    // for tabs
   $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };

    if ($stateParams.Id) {
      getItem($stateParams.Id);
    }
    else {
      getItems();
    }
  })