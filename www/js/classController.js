angular.module('starter.controllers')

.controller('ClassCtrl', function ($scope, $rootScope, $ionicPopup,   $stateParams, $http, $ionicModal, $ionicHistory, $state, moment, Calendar, $cordovaCalendar) {

  $scope.tableName = 'sinifs';
  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true

  function getSchedule(id) {
    // get class schedule
    var filter = {
      'classid': id
    };
    Db.getWithFilter('classtime', filter).then(function (result) {
      $scope.classtimes = result;
    }, function (error) {
      alert(error);
    });
  }

  function getAllTraininRoomsInTheSchool(id) {
    // get all training room in the school
    var roomfilter = {
      'schoolid': $rootScope.schoolid
    };
    Db.getWithFilter('room', roomfilter).then(function (result) {
      $scope.rooms = result;
    }, function (error) {
      alert(error);
    });
  }


  function getAllStudentsInTheClass(id) {
   Db.getChilds('sinifs', id, 'students', id).then(function (result) {
      $scope.classstudents = result;
    }, function (error) {
      alert(error);
    });
  }

  function getAllTechersInTheClass(id) {
   Db.getChilds('sinifs', 0, 'teachers', id).then(function (result) {
      $scope.classteachers = result;
    }, function (error) {
      alert(error);
    });
  }
 
  function getItem(id) {

    Db.get($scope.tableName, id).then(function (result) {
          $scope.item = result;
        },
        function (error) {
          alert(error);
        })
      .finally(function () {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });
    // getSchedule(id);
 
    getAllStudentsInTheClass(id);
    getAllTechersInTheClass(id);
 

    $scope.doRefresh = function () {
      getItem();
    };
  }

  $scope.addItem = function (item, schoolId) {
    item.schoolId = $rootScope.schoolid;
    item.thumbnailUrl = "img\class1.png";
    Db.add($scope.tableName, item).then(function (params) {
      var params = {
        Id: $rootScope.schoolid
      };
      $state.go('app.school', params, {reload: true, inherit: true, notify: true });
    });
  }

  $scope.deleteItem = function (item) {

    var tablename = 'class';
    var checklist = [{
      tablename: 'classteacher',
      propertyname: 'classidd',
      'id': item.id
    }, {
      tablename: 'classstudent',
      propertyname: 'classidd',
      'id': item.id
    }, ];

    Db.checkReleations(item.id, checklist).then(function (resp) {
        // this item does not any relation
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete class!',
          template: 'Are you sure you want to delete ' + item.name + ' ?'
        });

        // ask for delete
        confirmPopup.then(function (res) {
          if (res) {
            // first get the instance
            Db.get(tablename, item.id).then(function (result) {
                var obj = result;
                // then delete the instance
                Db.deleteWithId(tablename, obj).
                then(function (result) {
                    // get items.
                    //getItems();
                    //$ionicHistory.goBack(-2);
                    $state.go('app.school', {
                      Id: $rootScope.schoolid
                    }, {
                      reload: true
                    });
                  },
                  function (error) {
                    alert(error)
                  }
                )

              },
              function (error) {
                alert(error)
              })
          }
        })
      },
      function (error) {
        alert("cannot delete " + item.name);
      })

  }


  $scope.postponeNextLesson = function (item) {

    var confirmPopup = $ionicPopup.confirm({
      title: 'Postpone next lesson! ',
      template: 'Are you sure you want to postpone next lesson at  ' + item.day + ' - ' + item.time + ' - ' + item.room + ' ?'
    });

    confirmPopup.then(function (res) {
      if (res) {
        /*
                  var item = Db.get(id);
                  Db.delete($scope.tableName, item).then(function (result) {
                    // get all items.
                  }, function (error) {
                    alert(error);
                  }).finally(function () {
                    // Stop the ion-refresher from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                  })
                  */
      }
    });
  };

  $scope.addClassTime = function (classtime) {
    debugger;
    classtime.classid = $scope.classid;
    Db.add('classtime', classtime).then(function (result) {
      getSchedule($scope.classid);
      $scope.closeModal();
    }, function (error) {
      alert(error);
    }).finally(function () {

    })
  };

  /** Modal class time begin  */
  $ionicModal.fromTemplateUrl('add-class-time.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function () {

    $scope.modal.show();
  };
  $scope.closeModal = function () {
    $scope.modal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function () {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function () {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function () {
    // Execute action
  });
  /** Modal class time end  */

  $scope.shoutReset = function () {
    alert("Add new student");
  };

  $scope.selectStudent = function (student) {
    var classstudent = {
      classid: $scope.classid,
      studentid: student.id,
      discount: 0
    };
    Db.add('classstudent', classstudent).then(function (params) {
      getItem($scope.classid);
    });
  };

  $scope.selectTeacher = function (teacher) {
    var classteacher = {
      classid: $scope.classid,
      teacherid: teacher.id

    };
    Db.add('classteacher', classteacher).then(function (params) {

      getAllTechersInTheClass($scope.classid);

    });
  };

  $scope.addNewStudent = function () {
    $rootScope.classid = $scope.classid;
    $state.go('app.addstudent');
  }

  $scope.addNewTeacher = function () {
    $rootScope.classid = $scope.classid;
    $state.go('app.addteacher');
  }

  // for tabs
  $scope.tab = 1;

  $scope.setTab = function (newTab) {
    $scope.tab = newTab;
  };

  $scope.isSet = function (tabNum) {
    return $scope.tab === tabNum;
  };

  $scope.weekdays = Calendar.getWeekDays();


  $scope.addevent = function (classevent) {
    classevent.classid = $scope.classid;
    Db.add('event', classevent).then(function (result) {

        $scope.createEvent = function () {
          $cordovaCalendar.createEvent({
            title: event.name,
            location: 'The Moon',
            notes: 'Bring sandwiches',
            startDate: classevent.date,
            endDate: classevent.date
          }).then(function (result) {
            console.log("Event created successfully");
          }, function (err) {
            console.error("There was an error: " + err);
          });
        }
      },
      function (error) {
        alert(error);
      }).finally(function () {

    })
  };

  $scope.createEvents = function () {
    var start = moment();
    var end = moment().add(1, 'months');

    console.log(start.format());
    console.log(end.format());
    var tmp = [];
    var current = start;
    while (current.isBefore(end)) {
      //console.log(current.format('YYYY MM DD  -  dddd'));

      $scope.classtimes.forEach(function (item) {
        if (item.day == current.day()) {
          console.log(item.day);
          //  tmp.push(createEvent('Ders 1', 0, t(7, 30), t(9, 45), 'ion-mic-c', 'blue', Date()));
          var classevent = Calendar.createEvent($scope.classid, 'Ders 1', 0, t(7, 30), t(9, 45), 'ion-mic-c', 'blue', current);
          $scope.addevent(classevent);

        }
      });

      current.add(1, 'day');
    }
    console.log("ok");
  }

  if ($stateParams.Id) {
    $scope.classid = $stateParams.Id;
    getItem($stateParams.Id);

  } else {
    // getItems();
  }
})