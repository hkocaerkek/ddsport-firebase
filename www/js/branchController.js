angular.module('starter.controllers')

  .controller('BranchCtrl', function ($scope, $ionicPopup,   $rootScope, $stateParams, $http, $ionicHistory, $ionicModal) {

    $scope.customColors = {
      "White": "#FFFFFF",
      "Silver": "#C0C0C0",
      "Gray": "#808080",
      "Black": "#000000",
      "Red": "	#FF0000",
      "DarkOrange" :"#FF8C00",
      "Maroon": "#800000",
      "Brown" : "#8B4513", 
      "Yellow": "#FFFF00",
      "Olive": "#808000",
      "Lime": "#00FF00",
      "Green": "#008000",
      "Aqua": "#00FFFF",
      "Teal": "#008080",
      "Blue": "#0000FF",
      "Navy": "#000080",
      "Fuchsia": "#FF00FF",
      "Purple": "#800080"
    }

    $scope.tableName = 'branch';
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true;
    $scope.belt = {};

    $scope.isBeltAddeble = false;

    function getItem(id) {
      $rootScope.branchid = id;
      Db.get($scope.tableName, id).then(function (result) {
        $scope.item = result;
        getBelts();
        $rootScope.branchname = $scope.item.name;
        if (result.isBeltEnabled) {
          $scope.isBeltAddeble = true;
        }
      },
        function (error) { alert(error); })
        .finally(function () {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        })
    };

    function getItems() {

      Db.getAll($scope.tableName).then(function (result) {
        $scope.items = result;
      }, function (error) {
        alert(error);
      }).finally(function () {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      })
    };
    
    $scope.doRefresh = function () {
      getItems();

    };

    function getBelts() {
      // end get all teachers in the school
      var filter = { 'branchid': $scope.item.id };
      Db.getWithFilter('belt', filter).then(function (result) {
        $scope.belts = result;
      }, function (error) { alert(error); });
    }

    $scope.addItem = function (item) {
      item.thumbnailUrl = "img\\boy.jpg";
      Db.add($scope.tableName, item).then(function (params) {
        $ionicHistory.goBack();
        $state.go('app.branches', {}, { reload: true });
      });
    }

    $scope.deleteItem = function (id, name, surname) {

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete teacher!',
        template: 'Are you sure you want to delete ' + name + ' ' + surname + ' ?'
      });

      confirmPopup.then(function (res) {
        if (res) {

          var item = Db.get(id);
          Db.delete($scope.tableName, item).then(function (result) {
            // get all items.
          }, function (error) {
            alert(error);
          }).finally(function () {
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
          })
        }
      });
    };

    /** add belt begin  */

    $scope.saveBelt = function (belt) {
      belt.branchid = $scope.item.id;
      Db.add('belt', belt).then(function (params) {
        getBelts();
        $scope.closeModal();
        $scope.belt = {};
      });

    }
    $ionicModal.fromTemplateUrl('add-belt.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });
    $scope.openModal = function () {

      $scope.modal.show();
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
      // Execute action
    });
    /**add belt time end  */

    if ($stateParams.Id) {
      getItem($stateParams.Id);
    }
    else {
      getItems();
    }
  })