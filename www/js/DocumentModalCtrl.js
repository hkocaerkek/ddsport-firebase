angular.module('starter.controllers')

.controller('DocumentModalCtrl', function($scope, FirebaseDb, $ionicPopup, $state, $stateParams, $rootScope, $http, $ionicHistory, $cordovaCamera) {

    $scope.documentImage = "";
    $scope.documentType = "";

    $scope.documentTypes = [
        { "name": "Kimlik" },
        { "name": "Sağlık Raporu" },
        { "name": "Vize" },
        { "name": "Diploma" },
        { "name": "Diğer" }
    ];

    $scope.takePicture = function() {

        if (typeof Camera == 'undefined') {
            $scope.documentImage = $scope.documents[0].image;
            return;
        }

        var options = {
            quality: 100,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.documentImage = "data:image/jpeg;base64," + imageData;
        }, function(err) {
            // An error occured. Show a message to the user
            $scope.documentImage = $scope.documents[0];
        });
    }

    $scope.saveDocument = function(item, image, documentType) {
        item.name = documentType.name;
        item.image = $scope.documentImage;
        FirebaseDb.addDocuments('teachers', $scope.item.$id, item);
    }

})