angular.module('starter.controllers')

.controller('AdminCtrl', function ($scope, $ionicPopup, Db, $rootScope, $cordovaInAppBrowser) {

  function getShoools() {
    Db.getAll('schools').then(function (result) {
      $scope.schools = result;
    }, function (error) {
      alert(error.data.ExceptionMessage);
    }).finally(function () {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    })
  };

  function getBranches() {
    Db.getAll('branches').then(function (result) {
      $scope.branches = result;
    }, function (error) {
      alert(error.data.ExceptionMessage);
    }).finally(function () {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    })
  };

  $scope.doRefresh = function () {
    getShoools();
    getBranches();
  };

  // for tabs
  $scope.tab = 1;
  $scope.setTab = function (newTab) {
    $scope.tab = newTab;
  };
  $scope.isSet = function (tabNum) {
    return $scope.tab === tabNum;
  };

  $scope.doRefresh();


  var options = {
    location: 'no',
    clearcache: 'yes',
    toolbar: 'no'
  };

  $scope.openBrowser = function () {
      $cordovaInAppBrowser.open('http://ngcordova.com', '_blank', options)

      .then(function (event) {
        // success
      })

      .catch(function (event) {
        alert(1);
      });
    }
    //$scope.openBrowser();
})